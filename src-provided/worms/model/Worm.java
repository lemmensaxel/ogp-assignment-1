package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;

/**
 * A class of worms.
 * 
 * @invar	The X-coordinate of each worm must be a valid value.
 * 			| isValidCoordinate(getCoordinateX())
 * @invar 	The Y-coordinate of each worm must be a valid value.
 * 			| isValidCoordinate(getCoordinateY())
 * @invar	The direction of each worm must be a valid value.
 * 			| isValidDirection(getDirection())
 * @invar	The radius of each worm must be a valid value.
 * 			| isValidRadius(getRadius())
 * @invar	The name of each worm must be a valid value.
 * 			| isValidName(getName())
 * 
 * @version 1.0
 * @author Axel Lemmens & Maarten Rimaux 
 *
 */
public class Worm {
	
	//CONSTRUCTOR
	
	/**
	 * @param 		xCoordinate
	 * 				The provided value for the x-coordinate in meters.
	 * @param 		yCoordinate
	 * 				The provided value for the y-coordinate in meters.
	 * @param 		direction
	 * 				The provided value for the direction in radians.
	 * @param 		radius
	 * 				The provided value for the radius in meter.
	 * @param 		name
	 * 				The provided string for the name.
	 * @pre			The provided direction must be a valid value.
	 * 				| isValidDirection(direction)
	 * @post		The new value of the x-coordinate is equal to the provided value "xCoordinate"
	 * 				| new.getCoordinateX() == xCoordinate
	 * @post		The new value of the y-coordinate is equal to the provided value "yCoordinate"
	 * 				| new.getCoordinateY() == yCoordinate
	 * @post		The new value of the direction is equal to the provided value "direction"
	 * 				| new.getDirection() == direction
	 * @post		The new value of the radius is equal to the provided value "radius"
	 * 				| new.radius() == radius
	 * @post		The new value of the name is equal to the provided string "name"
	 * 				| new.name() == name
	 * @post		the new value of the action points is set to the maximum action points
	 * 				|new.getCurrentActionPoints() == getMaximumActionPoints
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(xCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(yCoordinate)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid radius.
	 * 				| ! isValidRadius(radius)
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid name.
	 * 				| ! isValidName(name)
	 */
	
	public Worm(double xCoordinate, double yCoordinate, double direction, double radius, String name) throws IllegalArgumentException {		
		this.setCoordinateX(xCoordinate);
		this.setCoordinateY(yCoordinate);
		this.setDirection(direction);
		this.setRadius(radius);
		this.setName(name);
		this.setCurrentActionPoints(getMaximumActionPoints());
	}
	
	//POSITION
	
	/**
	 * A method that returns the x-coordinate of the current position of this worm.
	 */
	@Basic
	public double getCoordinateX() {
		return xCoordinate;
	}
	
	/**
	 * A method that returns the y-coordinate of the current position of this worm.
	 */
	@Basic
	public double getCoordinateY() {
		return yCoordinate;
	}
	
	/**
	 * A method to set the x-coordinate of the position of this worm.
	 * 
	 * @param 		xCoordinate
	 * 				The new x-coordinate of the position of this worm.
	 * @post		The new value of the xCoordinate is set to the provided value.
	 * 				| new.getCoordinateX() == xCoordinate
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(xCoordinate) 
	 */
	public void setCoordinateX(double xCoordinate) throws IllegalArgumentException {
		if (! isValidCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * A method to set the y-coordinate of the position of this worm.
	 * 
	 * @param 		yCoordinate
	 * 				The new y-coordinate of the position of a worm.
	 * @post		The new value of the yCoordinate is set to the provided value.
	 * 				| new.getCoordinateY() == yCoordinate
	 * @throws		IllegalArgumentException
	 * 				An exception is thrown when the provided value is not a valid coordinate.
	 * 				| ! isValidCoordinate(yCoordinate)
	 */
	public void setCoordinateY(double yCoordinate) throws IllegalArgumentException { 
		if (! isValidCoordinate(yCoordinate))
			throw new IllegalArgumentException();	
		this.yCoordinate = yCoordinate;
		
	}
	
	/**
	 * A method to check whether a coordinate is valid.
	 * 
	 * @param 		aCoordinate
	 * 				The coordinate to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(aCoordinate) 
	 * 				| && (aCoordinate < Double.POSITIVE_INFINITY)
	 * 				| && (aCoordinate > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidCoordinate(double aCoordinate) { 
		return (!Double.isNaN(aCoordinate) 
				&& (aCoordinate < Double.POSITIVE_INFINITY) 
				&& (aCoordinate > Double.NEGATIVE_INFINITY));									 
	}
	
	/**
	 * Variable registering the x-coordinate of the position of this worm.
	 */
	private double xCoordinate;
	
	/**
	 * Variable registering the y-coordinate of the position of this worm.
	 */
	private double yCoordinate;
	
	
	// ORIENTATION / DIRECTION
	
	/**
	 * A method that returns the current value of the angel of this worm.
	 */
	@Basic
	public double getDirection() {
		return direction;
	}
	
	/**
	 * A method to set the direction of this worm to the provided value.
	 * 
	 * @param 		direction
	 * 				The new direction for the orientation of this worm.
	 * @pre			The provided value must be a valid value.
	 * 				| isValidDirection(direction)
	 * @post		The direction of this worm is set to the provided value.
	 * 				| new.getDirection() == direction
	 */
	public void setDirection(double direction) { 
		assert(isValidDirection(direction));
		this.direction = direction;
	}
	
	/**
	 * A method to check whether a direction is valid.
	 * 
	 * @param 		direction 
	 * 				The direction to check.
	 * @return 		The result should be a valid number.
	 * 				| result == !double.isNaN(direction) 
	 * 				| && (direction < Double.POSITIVE_INFINITY) 
	 * 				| && (direction > Double.NEGATIVE_INFINITY)
	 */
	public static boolean isValidDirection(double direction){
		return (!Double.isNaN(direction) 
				&& (direction < Double.POSITIVE_INFINITY) 
				&& (direction > Double.NEGATIVE_INFINITY));	
	}
	
	/**
	 * Variable registering the direction for the orientation of this worm.
	 */
	private double direction;
	
	// RADIUS
	
	/**
	 * A method that returns the current radius of this worm expressed in meters.
	 */
	@Basic
	public double getRadius() {
		return radius;
	}
	
	/**
	 * A method that returns the lower bound radius of this worm expressed in meters.
	 */
	@Basic
	public double getLowerBoundRadius() {
		return lowerBoundRadius;
	}
	
	/**
	 * A method that sets the radius of this worm to the provided value.
	 * 
	 * @param 		radius
	 * 				The new radius of this worm.
	 * @post 		The radius of this worm is set to the provided value.
	 * 				| new.getRadius() == radius
	 * @post		IllegalArgumentException
	 * 				The method must throw an exception when the provided value is not a valid radius.
	 * 				| !isValidRadius(radius)
	 */
	public void setRadius(double radius) throws IllegalArgumentException {
		if (! isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
		
	}
	
	/**
	 * A method to check whether a given value for radius is valid.
	 * 
	 * @param		radius
	 * 				The radius to check.
	 * @return		The result should be larger or equal to the lower bound of the radius.
	 * 				| result ==  (radius >= getLowerBoundRadius())
	 * @return 		The result must be a valid number.
	 * 				| result == double.isNaN(radius) 
	 * 				| && (radius < Double.POSITIVE_INFINITY)
	 * 				| && (radius > Double.NEGATIVE_INFINITY))
	 */
	public Boolean isValidRadius(double radius) {
		return (radius >= this.getLowerBoundRadius())
				&& (!Double.isNaN(radius) 
				&& (radius < Double.POSITIVE_INFINITY)
				&& (radius > Double.NEGATIVE_INFINITY));
	}
	
	/**
	 * Variable registering the radius of this worm.
	 */
	private double radius;
	
	/**
	 * Variable registering the lower bound radius of this worm.
	 */
	private double lowerBoundRadius = 0.25;
	
	//  MASS
	
	/**
	 * A method to return the mass of a worm expressed in kilogram.
	 * 
	 * @return 		The mass of this worm.
	 * 				| result ==	
	 * 				| this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3)))
	 */
	public double getMass() {
		return (this.getDensity()*((4.0/3.0)*(Math.PI)*(Math.pow(this.getRadius(),3))));
	}
	
	/**
	 * A method to return the density of this worm.
	 */
	@Basic @Immutable
	public double getDensity(){
		return 1062.0;
	}
	
	// ACTION POINTS
	
	/**
	 * A method that returns the maximum amount of action points of this worm.
	 * 
	 * @return 		The maximum amount of action of this worm.
	 * 				| result == (int)Math.round(this.getMass())
	 */
	public int getMaximumActionPoints() {
		return (int) Math.round(this.getMass());
	}
	
	/**
	 * A method that returns the minimum amount of action points of this worm.
	 */
	@Basic	@Immutable
	public int getMinimumActionPoints() {
		return 0;
	}
	
	/**
	 * A method that returns the current amount of action points of this worm.
	 */
	@Basic
	public int getCurrentActionPoints() {
		return currentActionPoints;
	}
	
	/**
	 * A method that sets the current action points on this worm to the provided value.
	 * 
	 * @param 		currentPoints
	 * 				The new current points for the action points of this worm.
	 * @post		If currentPoints is smaller or equal to the maximum value of points 
	 * 				and if currentPoints is larger or equal to the minimum value of points
	 * 				then currentPoints is set to the provided value.
	 * 				|if ((currentPoints <= getMaximumActionPoints()) 
	 * 				| && (currentPoints >= getMinimumActionPoints()))
	 * 				|	then new.getCurrentActionPoints() == currentPoints
	 * @post		If currentPoints is bigger than the maximum amount of action points, 
	 * 				currentPoints is set to the maximum amount of points.
	 * 				|if (currentPoints > getMaximumActionPoints())
	 * 				| 	then new.getCurrentActionPoints() == this.getMaximumActionPoints()
	 * @post		If the currentPoint is smaller than the minimum amount of action points, 
	 * 				currentPoints is set to the minimum amount of points.
	 * 				|if (currentPoints < getMinimumActionPoints())
	 * 				|	then new.getCurrentActionPoints() == this.getMinimumActionPoints()
	 */
	public void setCurrentActionPoints(int currentPoints) {
		if ( (currentPoints <= getMaximumActionPoints()) && (currentPoints >= getMinimumActionPoints()) )
				this.currentActionPoints = currentPoints;
		
		else if (currentPoints > getMaximumActionPoints()) {
				this.currentActionPoints = getMaximumActionPoints();
				}
		
		else if (currentPoints < getMinimumActionPoints()) {
				this.currentActionPoints =  getMinimumActionPoints();
				}
					
	}
	
	/**
	 * Variable registering the current action points of this worm.
	 */
	private int currentActionPoints;
	
	// NAME
	
	/**
	 * A method that returns the current name of this worm.
	 */
	@Basic
	public String getName() {
		return name;
	}
	
	/**
	 * A method that sets the name to a given string.
	 * 
	 * @param		name
	 * 				The new name of this worm.
	 * @post		The name of this worm is set to the provided value.
	 * 				| new.getName() == name
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the provided string is not valid.
	 * 				| (! isValidName(name))
	 */
	public void setName(String name) throws IllegalArgumentException {
		if ( ! isValidName(name))
			throw new IllegalArgumentException();
		this.name = name;
		
	}
	
	/**
	 * A method to check whether a given string for name is valid.
	 * 
	 * @param 		name
	 * 				The name to check.
	 * @return		The result should be a string starting with an uppercase letter, 
	 * 				at least two characters long and contains only letters, quotes and spaces. 
	 * 				| result ==  (string.matches("^[A-Z][a-zA-Z\\s\'\"]{2,}")
	 */
	public static boolean isValidName(String name) {
		return name.matches("^[A-Z][a-zA-Z\\s\'\"]{1,}");
	}
	
	/**
	 * Variable registering the name of this worm.
	 */
	private String name;
	
	//MOVING
	
	/**
	 * A method to check whether the number of steps is valid.
	 * 
	 * @param 		numberOfSteps
	 * 				The number of steps to check.
	 * @return		The result is true when the numberOfSteps is larger than zero.
	 * 				| result == numberOfSteps > 0
	 */
	public static boolean isValidNumberOfSteps(int numberOfSteps) {
		return (numberOfSteps > 0);
	}
	
	/**
	 * A method to check whether this worm has enough action points to move.
	 * 
	 * @param		numberOfSteps
	 * 				The number of steps to check the action points.
	 * @return		The result is true when the worm has enough action points to move.
	 * 				| result == ((getCurrentActionPoints() - (numberOfSteps*(requiredActionPoints()))) >= getMinimumActionPoints())
	 */
	public boolean hasEnoughActionPoints(int numberOfSteps) {
		return (this.getCurrentActionPoints() - (numberOfSteps*(requiredActionPoints()))) >= getMinimumActionPoints();
	}
	
	/**
	 * A method that returns the requiredActionPoints to move.
	 * 
	 * @return		The required action points to move.
	 * 				| Math.abs(Math.cos(getDirection()))+((Math.abs(4*Math.sin(getDirection()))))
	 */
    public double requiredActionPoints(){
    	return (Math.abs(Math.cos(getDirection()))+((Math.abs(4*Math.sin(getDirection())))));
    }
	
	/**
	 * A method that moves the worm with a given amount of steps.
	 * 
	 * @param 		numberOfSteps
	 * 				The number of steps to move this worm.
	 * @post		The x-coordinate is set to the new position.
	 * 				| new.getCoordinateX() == (Math.cos(getDirection())*getRadius()*numberOfSteps) + this.getCoordinateX
	 * @post		the y-coordinate is set to the  new position.
	 * 				| new.getCoordinateY() == (Math.sin(getDirection())*getRadius()*numberOfSteps) + this.getCoordinateY
	 * @post		The action points must be set equal to the current action points minus the action points required for the number of steps.
	 * 				| new.getCurrentActionPoints() == this.getCurrentActionPoints() - (numberOfSteps*(requiredActionPoints()))))
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the provided numberOfSteps is not valid.
	 * 				| (! isValidNumberOfSteps(numberOfSteps))
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the worm has not enough action points.
	 * 				| (! hasEnoughActionPoints(numberOfSteps)
	 * 
	 */
	public void move(int numberOfSteps) throws IllegalArgumentException {
		if (! isValidNumberOfSteps(numberOfSteps))
			throw new IllegalArgumentException();
		if(! hasEnoughActionPoints(numberOfSteps))
			throw new IllegalArgumentException();
		setCoordinateX((Math.cos(getDirection())*getRadius()*numberOfSteps)+this.getCoordinateX());
		setCoordinateY((Math.sin(getDirection())*getRadius()*numberOfSteps)+this.getCoordinateY());		
		setCurrentActionPoints((int)Math.round(this.getCurrentActionPoints() - (numberOfSteps*(requiredActionPoints()))));
		
	}
	
	// TURN
	
	/**
	 * A method that changes the direction of this worm with a given value.
	 * 
	 * @param 		direction
	 * 				The new direction of this worm.
	 * @pre			The provided value has to be a valid direction.
	 * 				|isValidDirection(direction)
	 * @pre			The worm must have enough action points.
	 * 				|canTurn(direction)
	 * @post		The new direction is equal to the current direction plus the provided value.
	 * 				| new.getDirection() == this.getDirection() + direction
	 * @post		The action points needed for the turn must be subtracted from the current action points.
	 * 				| new.getCurrentActionPoints() == getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction))) 
	 */
	public void turn(double direction) {
		assert isValidDirection(direction);
		assert canTurn(direction);
		setDirection(this.getDirection()+direction);
		setCurrentActionPoints((int)Math.ceil(this.getCurrentActionPoints() - Math.abs((60/((2*Math.PI)/direction)))));
	}
	
	/**
	 * A method to check whether this worm has enough action points to turn.
	 * 
	 * @return 		returns true when the used amount of action point, 
	 * 				subtracted from the current amount of action point is bigger than the minimum amount of action points.
	 * 				| result == Math.ceil(this.getCurrentActionPoints() - (direction / Math.abs((60/((2*Math.PI)/direction))))) >= this.getMinimumActionPoints()
	 */
	public boolean canTurn(double direction) {
		return (Math.ceil(this.getCurrentActionPoints() - Math.abs(60/((2*Math.PI)/direction))) >= this.getMinimumActionPoints());
	}
	
	//JUMP 
	
	/**
	 * A method that returns the force invoked on this worm.
	 * 
	 * @return		The force invoked on this worm.
	 * 				| result == (5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth())
	 */
	public double getForce(){
		return ((5*getCurrentActionPoints())+(getMass()*getAccelerationOfEarth()));
	}
	
	/**
	 * A method that returns the acceleration of the earth.
	 */
	@Immutable
	public double getAccelerationOfEarth(){
		return 9.80665;
	}
	
	/**
	 * A method that returns the velocity of this worm.
	 * 
	 * @return		The velocity of this worm.
	 * 				| result == (getForce()/getMass())*0.5
	 */
	public double getVelocity(){
		return (((getForce()/getMass()))*0.5);
	}
	
	/**
	 * A method that returns the distance of the jump.
	 * 
	 * @return		The distance of the jump of this worm.
	 * 				| result == (Math.pow(getVelocity(),2)*Math.sin(2*getDirection()))/getAccelerationOfEarth()
	 */
	public double getDistance(){
		return ((Math.pow(getVelocity(),2)*Math.sin(2*getDirection()))/getAccelerationOfEarth());
	}
	
	/**
	 * A method to check whether this worm can jump.
	 * 
	 * @return 		The method returns true if CurrentActionPoints is bigger than the minimum amount of action points 
	 * 				and if the simplified direction lays between zero and PI.
	 * 				| result == (this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
	 * 				| && (this.getSimplifiedDirection(this.getDirection()) < Math.PI) 
	 * 				| && (this.getSimplifiedDirection(this.getDirection()) > 0)
	 */
	public boolean canJump(){
		return ((this.getCurrentActionPoints() > this.getMinimumActionPoints()) 
				&& (this.getSimplifiedDirection(this.getDirection()) < Math.PI) 
				&& (this.getSimplifiedDirection(this.getDirection()) > 0));
	}
	/**
	 * A method that returns a simplified direction between 0 and 2*PI.
	 * 
	 * @param 		direction
	 * 				The direction that needs to be simplified.
	 * @return		The method returns the simplified direction.
	 * 				| result == direction < 2*Math.PI && direction > 0
	 */
	public double getSimplifiedDirection(double direction){
		while (direction > Math.PI*2) {
			direction = direction - 2*Math.PI;	
		}
		while(direction < 0) {
			direction = direction + 2*Math.PI;
		}
		return direction;
	}
	
	/**
	 * A method to let this worm jump.
	 * 
	 * @post		The x-coordinate is set to the old coordinate plus the distance.
	 * 				| new.getCoordinateX() == this.getCoordinateX() + this.getDistance()
	 * @post		The current action points are set to the minimum of action points.
	 * 				| new.getCurrentActionPoints() == this.getMinimumActionPoints()
	 * @throws		IllegalArgumentException
	 * 				The method throws the exception when the worm cannot jump.
	 * 				| (!this.canJump())
	 */
	public void jump(Worm worm) throws IllegalArgumentException{
		if(!this.canJump()){
			throw new IllegalArgumentException();
		}
		this.setCoordinateX(worm.getCoordinateX() + worm.getDistance());
		this.setCurrentActionPoints(this.getMinimumActionPoints());
	}
	
	/**
	 * A method that returns the time necessary to jump.
	 * 
	 * @return		The time necessary to jump.
	 * 				| result == (getDistance())/(getVelocity()*Math.cos(getDirection()))
	 */
	public double getJumpTime() {
		return ((getDistance())/(getVelocity()*Math.cos(getDirection())));
	}
	
	/**
	 * A method that computes in-flight positions X and Y coordinate at any time after launch of this worm.
	 * 
	 * @param 	time
	 * 			The time after the launch of the jump of this worm.
	 * @return	An array containing the new values for the X and Y coordinate at time.
	 *			|result == 
	 *			| new.getCoordinateX = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time))
	 *			| new.getCoordinateY = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2))
	 *			| double[] result = {new.getCoordinateX, new.getCoordinateY}
	 * @throws 	IllegalArgumentException
	 * 			The method throws the exception when the worm cannot jump.
	 * 			| (!this.canJump())
	 */
	public double[] jumpStep(double time) throws IllegalArgumentException{
		if(!canJump()){
			throw new IllegalArgumentException();
		}
		double x = this.getCoordinateX() + (((this.getVelocity()*Math.cos(this.getDirection()))*time));
		double y = this.getCoordinateY() + ((this.getVelocity()*Math.sin(this.getDirection())*time) - ((this.getAccelerationOfEarth()*Math.pow(time, 2))/2));
		double[] result = {x, y};
		
		return result;
	}
}
