package worms.model;


public class Facade implements IFacade{

	@Override
	public Worm createWorm(double x, double y, double direction, double radius,
			String name) {
		
		try {
		Worm worm = new Worm (x, y, direction, radius, name);
		
		return worm;
		
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public boolean canMove(Worm worm, int nbSteps) {
		try {
		return worm.isValidNumberOfSteps(nbSteps) && worm.hasEnoughActionPoints(nbSteps);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public void move(Worm worm, int nbSteps) {
		try {
		worm.move(nbSteps);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public boolean canTurn(Worm worm, double angle) {
		try {
		return worm.canTurn(angle);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public void turn(Worm worm, double angle) {
		try {
		worm.turn(angle);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public void jump(Worm worm) {
		try {
		worm.jump(worm);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public double getJumpTime(Worm worm) {
		try {
		return worm.getJumpTime();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public double[] getJumpStep(Worm worm, double t) {
		try {
		return worm.jumpStep(t);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public double getX(Worm worm) {
		try {
		return worm.getCoordinateX();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public double getY(Worm worm) {
		try {
		return worm.getCoordinateY();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public double getOrientation(Worm worm) {
		try {
		return worm.getDirection();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public double getRadius(Worm worm) {
		try {
		return worm.getRadius();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public void setRadius(Worm worm, double newRadius) {
		try {
		worm.setRadius(newRadius);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public double getMinimalRadius(Worm worm) {
		try {
		return worm.getLowerBoundRadius();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public int getActionPoints(Worm worm) {
		try {
		return worm.getCurrentActionPoints();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public int getMaxActionPoints(Worm worm) {
		try {
		return worm.getMaximumActionPoints();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public String getName(Worm worm) {
		try {
		return worm.getName();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}

	@Override
	public void rename(Worm worm, String newName) {
		try {
		worm.setName(newName);
		} catch (Exception e) {
		    throw new ModelException(e);
		}
		
	}

	@Override
	public double getMass(Worm worm) {
		try {
		return worm.getMass();
		} catch (Exception e) {
		    throw new ModelException(e);
		}
	}
	

}
