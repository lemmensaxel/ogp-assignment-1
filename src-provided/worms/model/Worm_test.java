package worms.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Worm_test {
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

	}

	@After
	public void tearDown() throws Exception {
	}
	
	// POSITION
	
	@Test
	public void constructor_test() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(1.0, testworm.getCoordinateX(), 0);
		assertEquals(1.0, testworm.getCoordinateY(), 0);
		assertEquals(4.0, testworm.getDirection(), 0);
		assertEquals(4.0, testworm.getRadius(), 0);
		assertEquals("Test", testworm.getName());
	}

	@Test
	public void isValidCoordinate_LegalCase() {

		
		assertEquals(true, Worm.isValidCoordinate(1.0));
	}
	
	@Test
	public void isValidCoordinate_IllegalCase() {
		
		assertEquals(true, Worm.isValidCoordinate(-1.0));
	}
	
	@Test
	public void getCoordinateX_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(1.0, testworm.getCoordinateX(), 0);
	}
	
	@Test
	public void getCoordinateY_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4, 4, "Test");
		
		assertEquals(1.0, testworm.getCoordinateY(), 0);
	}
	
	@Test
	public void setCoordinateX_LegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4, 4, "Test");
		
		testworm.setCoordinateX(3.0);
		
		assertEquals(3.0, testworm.getCoordinateX(), 0);
	}

	
	@Test
	public void setCoordinateY_LegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4, 4, "Test");
		
		testworm.setCoordinateY(3.0);
		
		assertEquals(3.0, testworm.getCoordinateY(), 0);
	}
	
	//DIRECTION
	
	@Test
	public void getDirection_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(4.0, testworm.getDirection(), 0);
	}
	
	
	@Test
	public void isValidDirection_LegalCase() {

		
		assertEquals(true, Worm.isValidDirection(1.0));
	}
	
	
	@Test
	public void setDirection_LegalCase() {

		
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test"); 
		testworm.setDirection(1.0);
		
		assertEquals(1.0, testworm.getDirection(), 0);
	}
	
	//RADIUS
	
	@Test
	public void getRadius_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(4.0, testworm.getRadius(), 0);
	}
	
	@Test
	public void getLowerBoundRadius_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(0.25, testworm.getLowerBoundRadius(), 0);
	}
	
	@Test
	public void isValidRadius_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(true, testworm.isValidRadius(1.0));
	}
	
	@Test
	public void isValidRadius_IllegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(false, testworm.isValidRadius(0.21));
	}
	
	@Test
	public void isValidRadius_IllegalCase2() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(false, testworm.isValidRadius(-0.25));
	}
	
	@Test
	public void setRadius_LegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4, 4, "Test");
		
		testworm.setRadius(3.0);
		
		assertEquals(3.0, testworm.getRadius(), 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setRadius_IllegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4, 4, "Test");
		
		testworm.setRadius(-1.0);
		
	}
	
	//MASS
	
	@Test
	public void getMass_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		
		assertEquals(69, testworm.getMass(), 1);
		
	}
	
	@Test
	public void getDensity_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals(1062.0, testworm.getDensity(), 0);
	}
	
	// ACTION POINTS
	
	@Test
	public void getMaximumActionPoints_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		
		assertEquals(70,testworm.getMaximumActionPoints());
		
		
	}
	
	@Test
	public void getMinimumActionPoints_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		
		assertEquals(0, testworm.getMinimumActionPoints());
	}
	
	@Test
	public void getCurrentActionPoints_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		
		assertEquals(70, testworm.getCurrentActionPoints());
	}
	
	@Test
	public void setCurrentActionPoints_LegalCase1() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		testworm.setCurrentActionPoints(500);
		
		assertEquals(500, testworm.getCurrentActionPoints());
		
	}
	
	@Test
	public void setCurrentActionPoints_LegalCase2() { 
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		testworm.setCurrentActionPoints(90);
		assertEquals(70, testworm.getCurrentActionPoints());
	}

	@Test
	public void setCurrentActionPoints_LegalCase3() { 
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		testworm.setCurrentActionPoints(-500);
		assertEquals(0, testworm.getCurrentActionPoints());
	}
	
	//NAME
	
	@Test
	public void getName_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		assertEquals("Test", testworm.getName());
	}
	
	@Test
	public void isValidName_LegalCase() {
		
		assertEquals(true, Worm.isValidName("Axel"));
	}
	
	@Test
	public void isValidName_IllegalCase1() {
		
		assertEquals(false, Worm.isValidName("axel"));
	}
	
	@Test
	public void isValidName_IllegalCase2() {
		
		assertEquals(false, Worm.isValidName("Axel^$$"));
	}
	
	@Test
	public void isValidName_IllegalCase3() {
		
		assertEquals(true, Worm.isValidName("Ax"));
	}
	
	@Test
	public void isValidName_IllegalCase4() {
		
		assertEquals(false, Worm.isValidName("34254"));
	}

	@Test
	public void isValidName_IllegalCase5() {
		
		assertEquals(true, Worm.isValidName("A\"xe\"l"));
	}
	
	@Test
	public void isValidName_IllegalCase6() {
		
		assertEquals(false, Worm.isValidName("Axel45678"));
	}
	
	@Test
	public void isValidName_IllegalCase7() {
		
		assertEquals(false, Worm.isValidName("^$^"));
	}
	
	@Test
	public void isValidName_IllegalCase8() {
		
		assertEquals(false, Worm.isValidName("A"));
	}
	
	@Test
	public void isValidName_IllegalCase9() {
		
		assertEquals(false, Worm.isValidName(""));
	}
	
	@Test
	public void setName_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		testworm.setName("ObjectOrientedProgrammer");
		
		assertEquals("ObjectOrientedProgrammer", testworm.getName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void setName_IllegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		
		testworm.setName("objectOrientedProgrammer");
	}
	
	//MOVING
	
	@Test
	public void isValidNumberOfSteps_LegalCase() {
		assertEquals(true, Worm.isValidNumberOfSteps(5));
		
	}
	
	@Test
	public void isValidNumberOfSteps_IllegalCase() {
		assertEquals(false, Worm.isValidNumberOfSteps(-5));
		
	}
	
	@Test
	public void hasEnoughActionPoints_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		testworm.setCurrentActionPoints(500);
		assertEquals(true, testworm.hasEnoughActionPoints(1));
		
	}
	
	@Test
	public void hasEnoughActionPoints_IllegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		testworm.setCurrentActionPoints(0);
		assertEquals(false, testworm.hasEnoughActionPoints(5));
		
	}
	
	@Test
	public void requiredActionPoints_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		assertEquals(3.7, testworm.requiredActionPoints(), 0.1);
		
	}
	
	@Test
	public void move_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(500);
		double coX = testworm.getCoordinateX();
		double coY = testworm.getCoordinateY();
		
		testworm.move(2);
		int points = testworm.getCurrentActionPoints();
		assertEquals(coX-5.3, testworm.getCoordinateX(), 0.1);
		assertEquals(coY-6.05, testworm.getCoordinateY(), 0.1);
		assertEquals(points, testworm.getCurrentActionPoints(), 0.1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void move_IllegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(500);		
		testworm.move(-2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void move_IllegalCase2() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(0);
		testworm.move(2);
	}
	
	// TURN
	
	@Test
	public void turn_LegalCase() {
		
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(500);
		double direction = testworm.getDirection();
		testworm.turn(2);
		assertEquals(direction+2, testworm.getDirection(), 0.1);
		assertEquals(500-19.01, testworm.getCurrentActionPoints(), 1);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void turn_IllegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");	
		testworm.move(2000000000);
	}
	
	@Test
	public void canTurn_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(500);
		assertEquals(true, testworm.canTurn(2));
	}
	
	@Test
	public void canTurn_IllegalCase() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		testworm.setCurrentActionPoints(0);
		assertEquals(false, testworm.canTurn(2));
	}
	
	// JUMP
	
	@Test
	public void getForce_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		testworm.setCurrentActionPoints(100);
		assertEquals(1031, testworm.getForce(), 1);
		
	}
	
	@Test
	public void getAccelerationOfEarth_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 4.0, "Test");
		assertEquals(9.80665, testworm.getAccelerationOfEarth(), 0);
	}
	
	@Test
	public void getVelocity_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		assertEquals(7.4, testworm.getVelocity(), 1);
	}
	
	@Test
	public void getDistance() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		assertEquals(5.5, testworm.getDistance(), 1);
	}
	
	@Test
	public void canJump_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, Math.PI/2, 4.0, "Test");
		testworm.setCurrentActionPoints(100);
		assertEquals(true, testworm.canJump());
	}
	
	@Test
	public void canJump_IllegalCase() {
		Worm testworm = new Worm(1.0, 1.0, Math.PI+1, 4.0, "Test");
		assertEquals(false, testworm.canJump());
		
	}
	
	@Test
	public void canJump_IllegalCase2() {
		Worm testworm = new Worm(1.0, 1.0, Math.PI, 4.0, "Test");
		assertEquals(false, testworm.canJump());
		
	}
	
	@Test
	public void canJump_IllegalCase3() {
		Worm testworm = new Worm(1.0, 1.0, 2*Math.PI, 4.0, "Test");
		assertEquals(false, testworm.canJump());
		
	}
	
	@Test
	public void jump_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, Math.PI/2, 0.25, "Test");
		double coX = testworm.getCoordinateX();
		testworm.jump(testworm);
		assertEquals(0+coX, testworm.getCoordinateX(), 1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void jump_IllegalCase() throws IllegalArgumentException {
		Worm testworm = new Worm(1.0, 1.0, 2*Math.PI, 4.0, "Test");	
		testworm.jump(testworm);
	}
	
	@Test
	public void getJumpTime_Case() {
		Worm testworm = new Worm(1.0, 1.0, 4.0, 0.25, "Test");
		assertEquals(-1.15, testworm.getJumpTime(), 1);
	}
	
	@Test
	public void jumpStep_LegalCase() {
		Worm testworm = new Worm(1.0, 1.0, Math.PI/2, 0.25, "Test");
		assertEquals(1.0, testworm.jumpStep(0.5)[0], 0.01);
		assertEquals(3.5, testworm.jumpStep(0.5)[1], 0.1);
	}
}